module git.sindominio.net/estibadores/wkd

go 1.17

require (
	github.com/go-ldap/ldap/v3 v3.1.8
	github.com/namsral/flag v1.7.4-pre
)

require github.com/go-asn1-ber/asn1-ber v1.3.1 // indirect
