FROM registry.sindominio.net/debian as builder

ARG BRANCH=master

RUN apt-get update && \
    apt-get install -y --no-install-recommends git golang ca-certificates

COPY *.go go.mod /wkd/
WORKDIR /wkd

run CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' .

FROM scratch

ENV TZ=Europe/Madrid

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /wkd/wkd /wkd

ENTRYPOINT ["/wkd"]
