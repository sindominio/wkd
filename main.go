package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/go-ldap/ldap/v3"
	"github.com/namsral/flag"
)

const (
	openPGPhashAtrribute = "openPGPhash"
	openPGPkeyAtrribute  = "openPGPkey"
	mailAtrribute        = "mail"
)

func main() {
	var (
		bind    = flag.String("bind", ":8080", "bind address for the web server")
		addr    = flag.String("addr", "localhost:389", "LDAP server address, either host:port or the absolute path to a UNIX socket")
		user    = flag.String("user", "", "Username to bind to the LDAP")
		pass    = flag.String("pass", "", "Password of the LDAP user")
		baseDN  = flag.String("baseDN", "", "LDAP baseDN for searches")
		filter  = flag.String("filter", "", "LDAP filter for searches")
		timeout = flag.Duration("timeout", 30*time.Second, "request timeout")
	)
	flag.String(flag.DefaultConfigFlagname, "/etc/wkd.conf", "Path to configuration file")
	flag.Parse()

	d := discoverer{
		addr:    *addr,
		network: "tcp",
		user:    *user,
		pass:    *pass,
		baseDN:  *baseDN,
		filter:  *filter,
	}
	if strings.HasPrefix(d.addr, "/") {
		d.network = "unix"
	}
	handler := Handler{
		Discover: d.Discover,
	}
	http.ListenAndServe(*bind, http.TimeoutHandler(&handler, *timeout, ""))
}

type discoverer struct {
	addr    string
	network string
	user    string
	pass    string
	baseDN  string
	filter  string
}

func (d *discoverer) Discover(ctx context.Context, hash, domain, localPart string) ([]byte, error) {
	entry, err := d.getLDAPentry(ctx, hash)
	if err != nil {
		return nil, err
	}

	mail := entry.GetAttributeValue(mailAtrribute)
	mailParts := strings.Split(mail, "@")
	if len(mailParts) != 2 {
		log.Println("Wrong mail format on the user record:", mail)
		return nil, ErrNotFound
	}
	if (localPart != "" && mailParts[0] != localPart) || mailParts[1] != domain {
		log.Printf("The requested email %s@%s doesn't match with the user found %s", localPart, domain, mail)
		return nil, ErrNotFound
	}

	key := entry.GetRawAttributeValue(openPGPkeyAtrribute)
	if len(key) == 0 {
		return nil, ErrNotFound
	}

	return key, nil
}

func (d *discoverer) connect(ctx context.Context) (*ldap.Conn, error) {
	var nconn net.Conn
	var err error

	// Use the net package to dial the connection with a timeout.
	deadline, hasDeadline := ctx.Deadline()
	if hasDeadline {
		nconn, err = net.DialTimeout(d.network, d.addr, time.Until(deadline))
	} else {
		// TODO: just panic.
		nconn, err = net.Dial(d.network, d.addr)
	}
	if err != nil {
		return nil, err
	}

	// Initialize the ldap.Conn on top of the raw network connection.
	conn := ldap.NewConn(nconn, false)
	conn.Start()
	if hasDeadline {
		conn.SetTimeout(time.Until(deadline))
	}

	// Authenticate via simple bind.
	bindReq := ldap.SimpleBindRequest{
		Username: d.user,
		Password: d.pass,
	}
	if _, err := conn.SimpleBind(&bindReq); err != nil {
		conn.Close()
		return nil, err
	}

	return conn, nil
}

func (d *discoverer) getLDAPentry(ctx context.Context, hash string) (*ldap.Entry, error) {
	searchAttributes := []string{mailAtrribute, openPGPkeyAtrribute}

	conn, err := d.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	searchRequest := ldap.NewSearchRequest(d.baseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&%s(%s=%s))", d.filter, openPGPhashAtrribute, ldap.EscapeFilter(hash)),
		searchAttributes,
		nil,
	)

	sr, err := conn.Search(searchRequest)
	if err != nil {
		return nil, err
	}

	switch len(sr.Entries) {
	case 1:
		entry := sr.Entries[0]
		return entry, nil
	case 0:
		return nil, ErrNotFound
	default:
		return nil, fmt.Errorf("Unexpected number of matches for the hash: %s %d", hash, len(sr.Entries))
	}
}
