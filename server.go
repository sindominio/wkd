// Copyright (c) 2017 emersion
// from https://github.com/emersion/go-openpgp-wkd/
package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
)

// Version is the WKD protocol version.
const Version = 12

// Base is the well-known base path for WKD.
const Base = "/.well-known/openpgpkey"

// ErrNotFound is returned when the directory doesn't contain a public key for
// the provided address.
var ErrNotFound = errors.New("wkd: not found")

// Handler is a HTTP WKD handler.
type Handler struct {
	// Discover retrieves keys for an address. If there's no key available for
	// this address, ErrNotFound should be returned.
	// The returned io.Reader should read the binary representation of an OpenPGP key
	Discover func(ctx context.Context, hash, domain, local string) ([]byte, error)
}

func (h *Handler) servePolicy(w http.ResponseWriter, r *http.Request) {
	writePolicy(w)
}

func (h *Handler) serveDiscovery(w http.ResponseWriter, r *http.Request, hash, domain, local string) {
	ctx := r.Context()
	pubkeys, err := h.Discover(ctx, hash, domain, local)
	if err == ErrNotFound {
		http.NotFound(w, r)
		return
	} else if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/octet-string")
	w.Write(pubkeys)
}

// ServeHTTP implements http.Handler.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if !strings.HasPrefix(r.URL.Path, Base) {
		http.NotFound(w, r)
		return
	}
	path := strings.TrimPrefix(r.URL.Path, Base)

	if path == "/policy" {
		h.servePolicy(w, r)
		return
	}

	query := r.URL.Query()
	localPart := query.Get("l")

	if strings.HasPrefix(path, "/hu/") {
		hash := strings.TrimPrefix(path, "/hu/")
		h.serveDiscovery(w, r, hash, r.Host, localPart)
		return
	}

	pathParts := strings.Split(path, "/")
	if len(pathParts) == 4 && pathParts[2] == "hu" {
		hash := pathParts[3]
		domain := pathParts[1]
		h.serveDiscovery(w, r, hash, domain, localPart)
		return
	}

	http.NotFound(w, r)
}

func writePolicy(w io.Writer) error {
	_, err := fmt.Fprintf(w, "protocol-version: %v\n", Version)
	return err
}
